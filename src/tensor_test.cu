#include <linalg/linalg.hpp>
#include <iostream>
#include <chrono>

using namespace linalg;

template <typename T>
typename std::enable_if<std::is_same<T, int>::value, void>::type print_type(){}


template <typename T>
void runb(const T& a)
{
    //std::cerr << a[0](1) << std::endl;
}

template <typename T> 
void run(const T& a)
{
    std::cerr << a[0].size(0) << std::endl;
    //std::cout << a[0](0,1) << std::endl;
    auto t = a[0];
    runb(t);
}

int main()
{   
    using namespace std::chrono;
    using size_type = cuda_backend::size_type;
    using index_type = cuda_backend::index_type;    
    using real_type = float;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    cuda_backend::initialise(0, 100);
    cuda_backend::allocate_ones<complex<real_type>>(1000);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<real_type> time_span = duration_cast<duration<real_type>>(t2 - t1);
    std::cerr << "It took me " << time_span.count() << " seconds." << std::endl;
    /*
    tensor<complex<real_type>, 2> _fa(3, 3, 0.0);

    _fa(0,0) = complex<real_type>(3.0, 1.0);
    _fa(0,1) = 2.0;
    _fa(0,2) = 5.0;
    _fa(1,0) = 1.0;
    _fa(1,1) =-6.0;
    _fa(1,2) = 7.0;
    
    tensor<complex<real_type>, 2> temp(trans(_fa));
    _fa = temp;
    */

    size_t n = 10; size_t m = 250; size_t k = 1024;
    tensor<complex<real_type>, 3, cuda_backend> ca(n, m, k, []__device__(int k, int i, int j, int n){return complex<real_type>(static_cast<real_type>(i*n+j+1+k), i);}, k);
    tensor<complex<real_type>, 3> _ca(ca);

    std::vector<complex<real_type>> vals(3);   vals[0] = 1.0;  vals[1] = 1.0;  vals[2] = 1.0;
    std::vector<index_type> colinds(3);     colinds[0] = 0; colinds[1] = 2; colinds[2] = 1;
    std::vector<index_type> rowptr(3);      rowptr[0] = 0;  rowptr[1] = 1;  rowptr[2] = 3;

    t1 = high_resolution_clock::now();
    csr_matrix<complex<real_type>, cuda_backend> mat(vals, colinds, rowptr);
    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<real_type>>(t2 - t1);
    std::cerr << "It took me " << time_span.count() << " seconds." << std::endl;
 
    std::vector<complex<real_type>> vals2(6);   vals2[0] = 1.0;  vals2[1] = 2.0;  vals2[2] = 3.0; vals2[3] = 4.0; vals2[4] = 5.0; vals2[5] = 6.0;
    std::vector<index_type> colinds2(6);     colinds2[0] = 0; colinds2[1] = 2; colinds2[2] = 1; colinds2[3] = 10; colinds2[4] = 6;  colinds2[5] = 12;
    std::vector<index_type> rowptr2(5);      rowptr2[0] = 0;  rowptr2[1] = 1;  rowptr2[2] = 5; rowptr2[3] = 5; rowptr2[4] = 6;


    tensor<complex<real_type>, 2, cuda_backend> mat_dense(k, k, []__device__(int i, int j){return i == j ? complex<real_type>(i+1.0, 1.0) : 0.0;});
    tensor<complex<real_type>, 2, cuda_backend> result_matrix(k, k);
    tensor<complex<real_type>, 2, cuda_backend> working_matrix(k, k);


    t1 = high_resolution_clock::now();
    result_matrix = matmul(mat_dense, conj(mat_dense));
    cudaDeviceSynchronize();

    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<real_type>>(t2 - t1);
    std::cerr << "It took me " << time_span.count() << " seconds to do it dense." << std::endl;

    tensor<complex<real_type>, 2> dresult_matrix(result_matrix);
    for(size_type i=0; i<10; ++i){std::cerr << dresult_matrix(i,i) << std::endl;}
   
    std::vector<std::tuple<index_type, index_type, complex<real_type>> > coo(k);
    size_type counter = 0;
    std::vector<complex<real_type>> dvals(k);
    for(size_type i=0; i<k; ++i)
    {
        coo[counter] = std::make_tuple(i, i, complex<real_type>(i+1.0));
        dvals[counter] = complex<real_type>(i+1.0);
        ++counter;
    }
    mat.init(coo);
    diagonal_matrix<complex<real_type>, cuda_backend> diagmat(dvals);

    t1 = high_resolution_clock::now();

    complex<real_type> alpha = 1.0;
    complex<real_type> beta = 0.0;
#ifdef LINALG_CUSPARSE_OLD

    cusparseMatDescr_t descrA, descrB;
    cusparse_safe_call(cusparseCreateMatDescr(&descrA));
    cusparse_safe_call(cusparseCreateMatDescr(&descrB));
    CALL_AND_HANDLE(cusparse_safe_call(cusparseCcsrmm(cuda_backend::_environment.cusparse_handle(), CUSPARSE_OPERATION_NON_TRANSPOSE, k, k, k, mat.nnz(), (const cuComplex*)(&alpha), descrA, (const cuComplex*)(mat.buffer()), (const int*)(mat.rowptr()), (const int*)(mat.colind()), (const cuComplex*)(mat_dense.buffer()), k, (const cuComplex*)(&beta), (cuComplex*)(result_matrix.buffer()), k)), "Fuck");
    cudaDeviceSynchronize();
#else
    cusparseDnMatDescr_t resmat, B;
    cusparseSpMatDescr_t A;

    const auto& const_mat = mat;
    cusparse_safe_call(cusparseCreateDnMat(&B, (int64_t)k, (int64_t)k, (int64_t)k, mat_dense.buffer(), CUDA_C_32F, CUSPARSE_ORDER_COL));
    cusparse_safe_call(cusparseCreateDnMat(&resmat, (int64_t)k, (int64_t)k, (int64_t)k, result_matrix.buffer(), CUDA_C_32F, CUSPARSE_ORDER_COL));
    cusparse_safe_call(cusparseCreateCsr(&A, (int64_t)mat.nrows(), (int64_t)mat.ncols(), (int64_t)mat.nnz(), (void*)const_mat.rowptr(), (void*)mat.colind(), (void*)mat.buffer(), CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_BASE_ZERO, CUDA_C_32F));

    size_t buffersize = 0;

    std::cerr << sizeof(csr_matrix<complex<real_type>, cuda_backend>::index_type) << std::endl;
    std::cerr << CUSPARSE_INDEX_32I << " " << CUSPARSE_INDEX_64I << std::endl;

    CALL_AND_HANDLE(cusparse_safe_call(cusparseSpMM(cuda_backend::_environment.cusparse_handle(), CUSPARSE_OPERATION_NON_TRANSPOSE, CUSPARSE_OPERATION_NON_TRANSPOSE, (cuComplex*)(&alpha), A, B, (cuComplex*)(&beta), resmat, CUDA_C_32F, CUSPARSE_MM_ALG_DEFAULT,  &buffersize)), "Fuck");
    cusparse_safe_call(cusparseDestroyDnMat(B));
    cusparse_safe_call(cusparseDestroyDnMat(resmat));
    cusparse_safe_call(cusparseDestroySpMat(A));
#endif

    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<real_type>>(t2 - t1);
    std::cerr << "It took me " << time_span.count() << " seconds to do it sparse." << std::endl;
    dresult_matrix = mat_dense;
    for(size_type i=0; i<10; ++i)
    {
        for(size_type j=0; j<10; ++j)
        {
            std::cerr << dresult_matrix(i,j) << " ";
        }
        std::cerr << std::endl;
    }

    dresult_matrix = result_matrix;
    for(size_type i=0; i<10; ++i)
    {
        for(size_type j=0; j<10; ++j)
        {
            std::cerr << dresult_matrix(i,j) << " ";
        }
        std::cerr << std::endl;
    }

    //coo[2] = std::make_tuple(1, 2, complex<real_type>(2.0));
    //coo[3] = std::make_tuple(1, 6, complex<real_type>(5.0));
    //coo[4] = std::make_tuple(1, 10, complex<real_type>(4.0));
    //coo[5] = std::make_tuple(3, 12, complex<real_type>(6.0));
    //mat.init(vals2, colinds2, rowptr2);
    //colinds2[0] = 0; colinds2[1] = 1; colinds2[2] = 2; colinds2[3] = 6; colinds2[4] = 10;  colinds2[5] = 12;
    //mat.set_colind(colinds2);

    //for(size_t i=0; i<_ca.shape(0); ++i)
    //{
    //    for(size_t j=0; j<_ca.shape(1); ++j)
    //    {
    //        for(size_t k=0; k<_ca.shape(2); ++k)
    //        {
    //            std::cout << _ca(i,j,k) << " ";
    //        }
    //        std::cout << std::endl;
    //    }
    //    std::cout << std::endl;
    //}
    return 0;
    auto rca = ca.reinterpret_shape(n, m, k);
    //tensor<complex<real_type>, 3, cuda_backend> dca(5, 2, 4, []__device__(int  k , int i, int j, int n){return complex<real_type>(static_cast<real_type>(i*n+j*k+1), i);}, 2 );
    tensor<complex<real_type>, 1, cuda_backend> conj_buffer(n*m*k, []__device__(int i){return static_cast<real_type>(i+1);});
    tensor<complex<real_type>, 1, cuda_backend> working_buffer(n*m*k);

    tensor<complex<real_type>, 3, cuda_backend> da(n, m, k, []__device__(int k , int i, int j, int n){return complex<real_type>(static_cast<real_type>(i*n+j+1+k), i);}, k);
    tensor<complex<real_type>, 3, cuda_backend> gfa;//(m, k, n);
    tensor<complex<real_type>, 3, cuda_backend> res(1, m, m);
    //for(size_t i=0; i<500; ++i)
    CALL_AND_HANDLE(
    {
        
        //res[0] = contract(ca, 0, 2, da, 0, 2, working_buffer, conj_buffer);
        res[0] = ca[1]*trans(da[0]);

        cudaDeviceSynchronize();
    }, "test");
    //_ca = ca;

    //for(size_t i=0; i<_ca.shape(0); ++i)
    //{
    //    for(size_t j=0; j<_ca.shape(1); ++j)
    //    {
    //        for(size_t k=0; k<_ca.shape(2); ++k)
    //        {
    //            std::cout << _ca(i,j,k) << " ";
    //        }
    //        std::cout << std::endl;
    //    }
    //    std::cout << std::endl;
    //}

    /*
    tensor<complex<real_type>, 3> _resf(ca);


    for(size_t i=0; i<_resf.size(0); ++i)
    {
        for(size_t j=0; j<_resf.size(1); ++j)
        {
            for(size_t k=0; k<_resf.size(2); ++k)
            {
                std::cerr << _resf(i, j, k) << " ";
            }
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }
    std::cerr << std::endl;
    std::cerr << std::endl;

    _resf = gfa;

    for(size_t i=0; i<_resf.size(0); ++i)
    {
        for(size_t j=0; j<_resf.size(1); ++j)
        {
            for(size_t k=0; k<_resf.size(2); ++k)
            {
                std::cerr << _resf(i, j, k) << " ";
            }
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }
    */
    ////cuda_backend::batched_transpose(false, 3, 4, complex<real_type>(1.0), ca.buffer(), gfa.buffer(), 5); 

    ////expression_templates::tensor_contraction_332<tensor<complex<real_type>, 3, cuda_backend>, tensor<complex<real_type>, 3, cuda_backend>> r(1.0, ca, dca, 1, working_buffer, conj_buffer, false, false);
    //tensor<complex<real_type>, 2, cuda_backend> resm = 0.5*contract(conj(ca), 0, 2, dca, 0, 2, working_buffer, conj_buffer);

    //tensor<complex<real_type>, 2, cuda_backend> tca(3, 4, []__device__(int i, int j, int n){return complex<real_type>(static_cast<real_type>(i*n+j+1), i);}, 2 );

    //tensor<complex<real_type>, 2, cuda_backend> fa(_fa);
    //
    ////tensor<complex<real_type>, 2, cuda_backend> resm(adjoint(fa)*tca);
    //tensor<complex<real_type>, 2> _resm(resm);

    //expression_templates::tensor_contraction_1_mt<tensor<complex<real_type>, 3, cuda_backend>, tensor<complex<real_type>, 2, cuda_backend> > r(1.0, ca, fa, 1, 0, false, true);
    //tensor<complex<real_type>, 3> _ca(ca);
    //for(size_t i=0; i<_ca.size(0); ++i)
    //{
    //    for(size_t j=0; j<_ca.size(1); ++j)
    //    {
    //        for(size_t k=0; k<_ca.size(2); ++k)
    //        {
    //            std::cerr << _ca(i, j, k) << " ";
    //        }
    //        std::cerr << std::endl;
    //    }
    //    std::cerr << std::endl;
    //}


    //tensor<complex<real_type>, 3> _gfa(gfa);
    //for(size_t i=0; i<_gfa.size(0); ++i)
    //{
    //    for(size_t j=0; j<_gfa.size(1); ++j)
    //    {
    //        for(size_t k=0; k<_gfa.size(2); ++k)
    //        {
    //            std::cerr << _gfa(i, j, k) << " ";
    //        }
    //        std::cerr << std::endl;
    //    }
    //    std::cerr << std::endl;
    //}
    //tensor<complex<real_type>, 3, cuda_backend> tempb = conj(5.0*ca);
    /*
    tensor<complex<real_type>, 3, cuda_backend> resf = contract(conj(fa), 0, ca, 1);

    tensor<complex<real_type>, 3> _resf(resf);


    for(size_t i=0; i<_resf.size(0); ++i)
    {
        for(size_t j=0; j<_resf.size(1); ++j)
        {
            for(size_t k=0; k<_resf.size(2); ++k)
            {
                std::cerr << _resf(i, j, k) << " ";
            }
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }
    */

    //for(size_t i=0; i<_resm.size(0); ++i)
    //{
    //    for(size_t j=0; j<_resm.size(1); ++j)
    //    {
    //        std::cerr << _resm(i, j) << " ";
    //    }
    //    std::cerr << std::endl;
    //}

    /*
    auto cat = ca.reinterpret_shape(5, 12);
    auto dcat = dca.reinterpret_shape(4, 12);
    tensor<complex<real_type>, 2, cuda_backend> resn(cat*adjoint(dcat));
    tensor<complex<real_type>, 2> _resn(resn);

    std::cerr << std::endl;
    for(size_t i=0; i<_resn.size(0); ++i)
    {
        for(size_t j=0; j<_resn.size(1); ++j)
        {
            std::cerr << _resn(i, j) << " ";
        }
        std::cerr << std::endl;
    }
    */
    cuda_backend::destroy();
#ifdef ZARYOLsudhjtarsyut
    using namespace std::chrono;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    //tensor<std::complex<real_type>, 3> a(1, 2000, 50, [](int /*k*/, int i, int j, std::complex<real_type> a){return (i < j ? -a*(i-j) : a*(i-j));}, 0.01);
    tensor<complex<real_type>, 3, cuda_backend> ca(1, 2000, 2000, [] __device__(int k, int i, int j, complex<real_type> a){return (i < j ? -a*static_cast<real_type>(i-j) : a*static_cast<real_type>(i-j));}, complex<real_type>(0, 0.01));
    tensor<complex<real_type>, 2, cuda_backend> fa(2000, 2000, [] __device__(int i, int j, complex<real_type> a){return (i < j ? -a*static_cast<real_type>(i-j) : a*static_cast<real_type>(i-j));}, complex<real_type>(0, 0.01));

    cuda_backend::initialise(0,6);
    /*
    tensor<complex<real_type>, 3, cuda_backend> rca(ca);

    tensor<complex<real_type>, 3, cuda_backend> da(12, 500, 2000);
    auto rda = da.reinterpret_shape(12, 1, 500, 2000);

    std::cerr << cuda_environment::list_devices << std::endl;

    cuda_environment cuback;
    std::cerr << cuback << std::endl;

    //tensor<std::complex<real_type>, 3> a(ca);
    cuda_backend::async_for(0, 12, 
        [&rda, &ca, &rca](int i)
        {
            rda[i] = (ca + rda[i]) - rca + (ca+rca) + 2.0*ca*3.0f + rca - rca - rca*50.0f;
            //rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2.0*ca*3.0f + rca - rca - rca*50.0f;
            //rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2.0*ca*3.0f + rca - rca - rca*50.0f;
            //rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2.0*ca*3.0f + rca - rca - rca*50.0f;
            //rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2.0*ca*3.0f + rca - rca - rca*50.0f;
        }
    );
    //ca = conj(ca);
    rda[0] = conj(conj(rda[0]));
    //tensor<complex<real_type>, 2, cuda_backend> transpose(trans(trans(ca[0])));
    tensor<complex<real_type>, 2, cuda_backend> transpose;
    

    tensor<complex<real_type>, 2> ba(transpose);
    tensor<complex<real_type>, 3> a(ca);
    */
    tensor<complex<real_type>, 2, cuda_backend> t5(ca[0]);
    expression_templates::matrix_matrix_product<tensor<complex<real_type>, 2, cuda_backend>, tensor<complex<real_type>, 2, cuda_backend>> m(1.0, fa, t5);
    tensor<complex<real_type>, 2, cuda_backend> resm(m);   
    resm = m;
    resm = m;
    resm = m;
    resm = m;
    resm = m;
    resm = m;
    resm = m;
    resm = m;


    //ca = conj(conj(ca));
    //for(size_t i=0; i<2; ++i)
    //{
    //    rda[i] = (ca + rca) - rca + (ca+rca) + 2*ca*3.0f + rca - rca - rca*50.0f;
    //    rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2*ca*3.0f + rca - rca - rca*50.0f;
    //    rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2*ca*3.0f + rca - rca - rca*50.0f;
    //    rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2*ca*3.0f + rca - rca - rca*50.0f;
    //    rda[i] = rda[i] + (ca + rca) - rca + (ca+rca) + 2*ca*3.0f + rca - rca - rca*50.0f;
    //}

    tensor<complex<real_type>, 2> res(resm);
    //tensor<complex<real_type>, 3> res(da);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<real_type> time_span = duration_cast<duration<real_type>>(t2 - t1);
    std::cerr << "It took me " << time_span.count() << " seconds." << std::endl;


    cuda_backend::destroy();

    //for(size_t i=0; i<a.size(0); ++i)
    //{
    //    for(size_t j=0; j<a.size(1); ++j)
    //    {
    //        for(size_t k=0; k<a.size(2); ++k)
    //        {
    //            std::cout << "[" << a(i, j, k) << "," << res(i, j, k) << "] ";
    //        }
    //        std::cout << std::endl;
    //    }
    //    std::cout << std::endl;
    //}
    /*
    const tensor<std::complex<real_type>, 2> b(20, 500, [](int i, int j, std::complex<real_type> a){return (i < j ? -a*(i-j) : a*(i-j));}, 0.1); 
    std::cerr << b[0](0) << std::endl;

    std::cerr << a.size() << std::endl;
    auto temp = ca.reinterpret_shape(10,20,10, 50);
    std::cerr << temp.size() << std::endl;
    auto temp2 = temp.reinterpret_shape(100, 20, 50);

    for(size_t i=0; i<temp.rank(); ++i){std::cerr << temp.shape(i) << std::endl;}

    const tensor<std::complex<real_type>, 3> c(100, 20, 50, [](int i, int j, int k, std::complex<real_type> a){return i*j+k*a;}, 0.1);

    //for(size_t i=0; i<20; ++i)
    //{
    //    for(size_t j=0; j<50; ++j)
    //    {
    //        std::cerr << "[ " << temp2[0](i,j) << " " << c[5](i,j)  << "] ";
    //    }
    //    std::cerr << std::endl;
    //}
    temp2[0] = c[5];
    tensor<std::complex<real_type>, 2> d(temp2[0]);
    for(size_t i=0; i<20; ++i)
    {
        for(size_t j=0; j<50; ++j)
        {
            std::cerr << "[ " << d(i,j) << " " << c[5](i,j)  << "] ";
        }
        std::cerr << std::endl;
    }

    auto temp3 = temp[0].reinterpret_shape(10, 20, 50);
    auto temp4 = temp3[1].reinterpret_shape(10, 2, 50);

    //std::cerr << temp[0].buffer() << " " << temp3.buffer() << std::endl;
    //std::cerr << temp3[1].buffer() << " " << temp4.buffer() << std::endl;
    //std::cerr << temp3.size(0) << " " << temp3.size(1) << " " << temp3.size(2) << std::endl;

    //std::cerr << temp[0](1,3,5) << " " << temp[0][1][3][5] << " " << temp(0, 1, 3, 5) << " " << a(10*50 + 3*50 + 5) << std::endl;
    //temp(0, 1, 1, 5) = 10.0;
    //std::cerr << temp2(1, 1, 5) << " " << a(10*100 + 50 + 5) << std::endl;
    std::cerr << temp.rank() << std::endl;

    for(size_t i=0; i<50; ++i){std::cerr << a(0,0,i) << " ";}   std::cerr << std::endl;
    a[0].fill( [](int i, int j, std::complex<real_type> a){return (i < j ? -a*(i-j) : a*(i-j));}, 0.1);
    for(size_t i=0; i<50; ++i){std::cerr << a(0,0,i) << " ";}   std::cerr << std::endl;
    
    std::cout << &a << std::endl;
    run(temp2);
    std::cout << &a << std::endl;
    std::cout << a(0,0,0) << std::endl;

    //tensor<std::complex<real_type>, 3> da(ca);
    //tensor<std::complex<real_type>, 3> e(2, 10, 50, [](int n, int i, int j){return (i < j ? -5.0*(i-j) + n : 5.0*(i-j)+n);});    
//    e[0] = a;
    //a.swap_buffer(d);
    //d.swap_buffer(a);
    
    //a = ca;

    //tensor<std::complex<real_type>, 2> b(a);
    //tensor<std::complex<real_type>, 1> d(10*50);
    //tensor<std::complex<real_type>, 2> c;    c = a;

    //for(size_t i=0; i<e.size(0); ++i){for(size_t j=0; j<e.size(1); ++j){for(size_t k=0; k<e.size(2); ++k){for(size_t l=0; l<e.size(3); ++l){std::cerr << e[i][j](k,l) << " ";} std::cerr << std::endl;} std::cerr << std::endl;} std::cerr << std::endl;}

    
    //for(size_t i=0; i<a.size(1); ++i)
    //{
    //    for(size_t j=0; j<a.size(2); ++j){std::cerr << a(0, i,j) -da(0,i,j) << " " ;}
    //    std::cerr << std::endl;
    //}
    */
#endif
}
