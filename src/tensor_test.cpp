#include <linalg/linalg.hpp>
#include <linalg/decompositions/eigensolvers/eigensolver.hpp>
#include <linalg/decompositions/singular_value_decomposition/singular_value_decomposition.hpp>
#include <linalg/decompositions/sparse/arnoldi_iteration.hpp>

#include <iostream>
#include <chrono>
#include <type_traits>
#include <fstream>
#include <typeinfo>
#include <random>

/*  
#ifdef CEREAL_LIBRARY_FOUND
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>
#endif
*/

using namespace linalg;

//#define LEFT_MULTIPLY
//#define TRANS_B
//#define PRINT_MATRICES

/*
void do_matrix_products(size_t m, size_t n, size_t k)
{
    using namespace std::chrono;
    using index_type = blas_backend::index_type;
    using size_type = blas_backend::size_type;

#ifdef LEFT_MULTIPLY
    tensor<complex<double>, 2> diag_mat_dense(m, k, [](int i, int j){return i == j ? complex<double>(i+1.0, 1.0) : 0.0;});
#ifdef TRANS_B
    tensor<complex<double>, 2> B(n, k, [](int i, int j, int n){return complex<double>(i*n+j+1.0, -1.0);}, n);
#else
    tensor<complex<double>, 2> B(k, n, [](int i, int j, int n){return complex<double>(i*n+j+1.0, -1.0);}, n);
#endif
    size_type min_km = k < m ? k : m;
#else
    tensor<complex<double>, 2> diag_mat_dense(k, n, [](int i, int j){return i == j ? complex<double>(i+1.0, 1.0) : 0.0;});
#ifdef TRANS_B
    tensor<complex<double>, 2> B(k, m, [](int i, int j, int n){return complex<double>(i*n+j+1.0, -1.0);}, m);
#else
    tensor<complex<double>, 2> B(m, k, [](int i, int j, int n){return complex<double>(i*n+j+1.0, -1.0);}, m);
#endif

    size_type min_kn = k < n ? k : n;
#endif
    tensor<complex<double>, 2> result_matrix(m,n);
    tensor<complex<double>, 2> result_matrix_dense(m,n);

#ifdef LEFT_MULTIPLY
    size_type diagsize = min_km;
#else
    size_type diagsize = min_kn;
#endif
    std::vector<complex<double>> dvals(diagsize);
    std::vector<complex<double>> devals(diagsize*2-1);
    for(size_type i=0; i<diagsize; ++i)
    {
        dvals[i] = complex<double>(i+1.0, 1.0);
        devals[i] = complex<double>(i+1.0, 1.0);
        if(i+1 != diagsize) 
        {
            devals[diagsize+i] = complex<double>(0, i+1);
        }
    }
#ifdef LEFT_MULTIPLY
    diagonal_matrix<complex<double>> diagmat(dvals, m, k);
#else
    diagonal_matrix<complex<double>> diagmat(dvals, k, n);
#endif
    symmetric_tridiagonal_matrix<complex<double>> symtrimat(devals);

    std::cout << diagmat << std::endl;
    std::cout << symtrimat << std::endl;
    return;

#ifdef PRINT_MATRICES
    tensor<complex<double>, 2> ddiag_mat_dense(diag_mat_dense);
    for(size_type i=0; i<diag_mat_dense.shape(0); ++i)
    {
        for(size_type j=0; j<diag_mat_dense.shape(1); ++j)
        {
            std::cerr << ddiag_mat_dense(i,j) << " ";
        }
        std::cerr << std::endl;
    }

    tensor<complex<double>, 2> dB(B);
    for(size_type i=0; i<B.shape(0); ++i)
    {
        for(size_type j=0; j<B.shape(1); ++j)
        {
            std::cerr << dB(i,j) << " ";
        }
        std::cerr << std::endl;
    }
#endif

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

#ifdef LEFT_MULTIPLY

#ifdef TRANS_B
    result_matrix_dense = diag_mat_dense*trans(B);
#else
    result_matrix_dense = diag_mat_dense*B;
#endif

#else
#ifdef TRANS_B
    result_matrix_dense = matmul(trans(B), diag_mat_dense);
#else
    result_matrix_dense = B*diag_mat_dense;
#endif
#endif

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    //std::cerr << "dense timing: " <<  time_span.count() << std::endl;
    double dense_time = time_span.count();
    tensor<complex<double>, 2> dresult_matrix_dense(result_matrix_dense);
#ifdef PRINT_MATRICES
    std::cerr << "dense result" << std::endl;
    for(size_type i=0; i<dresult_matrix_dense.shape(0); ++i)
    {
        for(size_type j=0; j<dresult_matrix_dense.shape(1); ++j)
        {
            std::cerr << dresult_matrix_dense(i,j) << " ";
        }
        std::cerr << std::endl;
    }
#endif
  

    t1 = high_resolution_clock::now();
    complex<double> alpha = 1.0;    
#ifdef LEFT_MULTIPLY

#ifdef TRANS_B
    t1 = high_resolution_clock::now();
    expression_templates::matrix_matrix_product<diagonal_matrix<complex<double>>, tensor<complex<double>, 2> > matmul_obj(alpha, diagmat, B, false, true);
    matmul_obj.applicative_impl(result_matrix);
    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double>>(t2 - t1);
    //std::cerr << "sparse timing dynamic: " <<  time_span.count() << std::endl;
#else
    t1 = high_resolution_clock::now();
    expression_templates::matrix_matrix_product<diagonal_matrix<complex<double>>, tensor<complex<double>, 2> > matmul_obj(alpha, diagmat, B);
    matmul_obj.applicative_impl(result_matrix);
    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double>>(t2 - t1);
    //std::cerr << "sparse timing dynamic: " <<  time_span.count() << std::endl;
#endif

#else

#ifdef TRANS_B
    t1 = high_resolution_clock::now();
    expression_templates::matrix_matrix_product<diagonal_matrix<complex<double>>, tensor<complex<double>, 2> > matmul_obj(alpha, B, diagmat, true);
    matmul_obj.applicative_impl(result_matrix);
    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double>>(t2 - t1);
    //std::cerr << "sparse timing dynamic: " <<  time_span.count() << std::endl;
#else
    t1 = high_resolution_clock::now();
    expression_templates::matrix_matrix_product<diagonal_matrix<complex<double>>, tensor<complex<double>, 2> > matmul_obj(alpha, B, diagmat);
    matmul_obj.applicative_impl(result_matrix);
    t2 = high_resolution_clock::now();
    time_span = duration_cast<duration<double>>(t2 - t1);
    //std::cerr << "sparse timing dynamic: " <<  time_span.count() << std::endl;
#endif

#endif
    double sparse_time = time_span.count();
    std::cerr << "sparse timing: " <<  sparse_time << std::endl;
    std::cerr << "dense timing: " <<  dense_time << std::endl;
    std::cerr << "ratio: " << dense_time/sparse_time << std::endl;

    //t2 = high_resolution_clock::now();
    //time_span = duration_cast<duration<double>>(t2 - t1);
    //std::cerr << "sparse timing: " <<  time_span.count() << std::endl;

    tensor<complex<double>, 2> dresult_matrix = result_matrix;
#ifdef PRINT_MATRICES
    std::cerr << "diagonal result" << std::endl;
    for(size_type i=0; i<dresult_matrix.shape(0); ++i)
    {
        for(size_type j=0; j<dresult_matrix.shape(1); ++j)
        {
            std::cerr << dresult_matrix(i,j) << " ";
        }
        std::cerr << std::endl;
    }
        std::cerr << std::endl;
        std::cerr << std::endl;
#else
    for(size_type i=0; i<dresult_matrix.shape(0); ++i)
    {
        for(size_type j=0; j<dresult_matrix.shape(1); ++j)
        {
            if(abs(dresult_matrix_dense(i, j) - dresult_matrix(i,j)) > 1e-15)
            {
                std::cerr << i << " " << j << dresult_matrix_dense(i, j) << " " << dresult_matrix(i,j) << std::endl;
            }
        }
    }
#endif

}*/

void test(const tensor<double, 2>& a)
{
    matrix<complex<double>> X, Y, temp, res;  diagonal_matrix<complex<double>> S;
    eigensolver<decltype(a)> solver;

    solver(a, S, X, Y);
    temp = S*adjoint(Y);
    res = X*temp;
    std::cout << a << std::endl;
    std::cout << S << std::endl;
    std::cout << X << std::endl;
    std::cout << res << std::endl;
}

template <typename Func>
void test_mul(double a, double b, Func&& f)
{
    return f(a, b);
}

int main()
{   
    {
        size_t m = 16;
        size_t n = 24;
        size_t k = 127;
        //arnoldi_iteration<double> arn(m, n*n);

        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937 rng(seed);
        std::uniform_real_distribution<double> dist(0.0,1.0);

        matrix<complex<double>> a(m, k, [n](size_t i, size_t j){return complex<double>(i*n+j+1.0, 1.0);}) ;
        matrix<complex<double>> v(n, m);//(n, n, [n](size_t i, size_t j){return i*n+j+1.0;}) ;

        std::vector<std::tuple<int, int, complex<double> > > coo( (n*(n+1))/2);
        matrix<complex<double>> densemat(n, k, 0.0);
        size_t counter = 0;
        for(size_t i=0; i<n; ++i)
        {
            for(size_t j=i; j<n; ++j)
            {
                coo[counter] = std::make_tuple(i, j, complex<double>(counter+1.0, 5.0-counter));
                densemat(i, j) = complex<double>(counter+1.0, 5.0-counter);
                ++counter;
            }
        }
        csr_matrix<complex<double>> csrmat(coo, k);
        vector<complex<double>> vec(k, [](size_t i){return complex<double>(i+1.0, 5.0-i);});
        diagonal_matrix<double> diagmat(k, k);
        for(size_t i=0; i<k; ++i){diagmat[i] = 1.0;}

        vector<complex<double>> vd = conj(diagmat)*conj(vec);
        vector<complex<double>> vd2 = conj(densemat)*conj(vec);

        matrix<complex<double>> vdm(1, vd.size());
        vdm[0] = vd;

        std::cerr << "-= test" << std::endl;
        std::cerr << vd << std::endl;
        vdm[0] -= conj(diagmat)*conj(vec);
        //vector<complex<double>> vf = vd2 - vd;
        std::cerr << vdm[0] << std::endl;
        std::cerr << vec << std::endl;

        
        /*
        //vector<double> v(n, [](size_t i){return i+1.0;});
        //vector<double> res = a*v;
        //std::cerr << res << std::endl;
        arn(2.0*a, v);



        std::cerr << arn.Q() << std::endl;
        std::cerr << arn.H() << std::endl;
        */
        //auto temp = arn.H();
        //{
        //    matrix<complex<double>> X, Y;    diagonal_matrix<complex<double> > vals;
        //    eigensolver<decltype(temp)> eigs;
        //    eigs(temp, vals, X, Y);
        //    std::cerr << vals << std::endl;
        //}
        //{
        //    matrix<complex<double>> X, Y;    diagonal_matrix<complex<double> > vals;
        //    eigensolver<decltype(a)> eigs;
        //    eigs(a, vals, X, Y);
        //    vector<complex<double>> vc(n, [](size_t i){return i+1.0;});
        //    vals.resize(n, 12);
        //    std::cerr << vals << std::endl;
        //    std::cerr << vc << std::endl;
        //    vector<complex<double>> res = adjoint(vals)*vc;
        //    std::cerr << res << std::endl;
        //}
    }

    

    /*  
    size_t n = 4
    symmetric_tridiagonal_matrix<double> a(n, n);
    for(size_t i=0; i<n; ++i){a(i, i) = i+1.0;}
    for(size_t i=0; i<n-1; ++i){a(i, i+1) = n+i+1.0;}
    matrix<double> m, temp, res;   diagonal_matrix<double> S;
    std::cout << a << std::endl;

    eigensolver<decltype(a)> solver;
    solver(a, S, m);
    std::cout << S << std::endl;
    std::cout << m << std::endl;
    temp = S*adjoint(m);
    res = m*temp;
    std::cout << res << std::endl;

    size_t m = 4;
    size_t n = 3;
    //double counter = 0.0;
    //tensor<double,3> a(1,n, n, [&counter](size_t k, size_t i, size_t j){if(j+2 > i){++counter; return counter;}else{return 0.0;}});

    matrix<double> a(m, n, [m](size_t i, size_t j){return i*m+j+1.0;});
    vector<double> v(m, [](size_t i){return i+1.0;});

    vector<double> working(m);
    vector<double> res = 2.0*adjoint(a)*(v);
    vector<double> op_v = (v);
    matrix<double> op_a = adjoint(a);
    vector<double> rop = 2.0*op_a*op_v;
    //std::cout << op_a << std::endl;
    //std::cout << op_v << std::endl;
    std::cout << res << std::endl;
    std::cout << rop << std::endl;
    //test(a);
    //auto hess = upper_hessenberg_view(a);
    */
    /*  
    solver(hess, S, X, Y);
    temp = S*adjoint(Y);
    res = X*temp;
    std::cout << a << std::endl;
    std::cout << S << std::endl;
    std::cout << X << std::endl;
    std::cout << Y << std::endl;
    std::cout << res << std::endl;
*/

    {
        size_t n = 16;   size_t m = 2;
        singular_value_decomposition<matrix<double>, true> svd;
        matrix<double> a(n, m, [m](size_t i, size_t j){return double(m*i+j+1.0);});
        matrix<double> U, V, Up, temp, R;    diagonal_matrix<double> S;
        auto ordering = svd(a, S, V);
        //std::cout << a << std::endl;
        //std::cout << S << std::endl;

        if(ordering == svd_result_ordering::usv)
        {
            std::cerr << "usv" << std::endl;
            temp = S*V;
            R = a*temp;
        }
        else
        {
            std::cerr << "vsu" << std::endl;
            temp = S*a;
            R = V*temp;
        }
        std::cout << R << std::endl;

        //U = U + U;
        svd(a, S, Up, V, false);
        diagonal_matrix<double > Sc(S);
        std::cout << "svd worked" << std::endl;
        std::cout << a << std::endl;
        std::cout << S << std::endl;
        std::cout << Up << std::endl;
        std::cout << V << std::endl;

        std::cerr << a.shape(0) << " " << a.shape(1) << std::endl;
        std::cerr << S.shape(0) << " " << S.shape(1) << std::endl;
        std::cerr << Up.shape(0) << " " << Up.shape(1) << std::endl;
        std::cerr << V.shape(0) << " " << V.shape(1) << std::endl;

        temp = Sc*V;
        R = Up*temp;
        matrix<double> res = R - a;
        for(size_t i=0; i<res.size(); ++i){res(i) = (std::abs(res(i)) < 1e-10 ? 0.0 : res(i));}
        std::cout << res << std::endl;
    }
    /*  
    size_t n=4;


    std::vector<double> array_buffer(2*n-1);    for(size_t i=0; i<array_buffer.size(); ++i){array_buffer[i] = i+1;}
    tensor<double, 2> resa(n, n);
    for(size_t i=0; i<n; ++i){resa(i, i) = array_buffer[i];}
    for(size_t i=0; i<n-1; ++i){resa(i+1, i) = array_buffer[i+n];   resa(i, i+1) = array_buffer[i+n];}
    symmetric_tridiagonal_matrix<double> mat(array_buffer);
    diagonal_matrix<double> vals;    tensor<double, 2> vecs;
    

    eigensolver<symmetric_tridiagonal_matrix<double> > decomp;

    //std::cerr << mat << std::endl;
    decomp(mat, vals, vecs);
    //std::cerr << mat << std::endl;
    std::cerr << vals << std::endl;
    std::cerr << vecs << std::endl;
    std::cerr << std::endl;
    
    tensor<double, 2> temp = vals*trans(vecs);
    tensor<double, 2> res = vecs*temp;
    for(size_t i=0; i<res.size(0); ++i)
    {
        for(size_t j=0; j<res.size(1); ++j)
        {
            if(std::abs(res(i,j)) < 1e-12){res(i, j) =0.0;}
        }
    }
    auto herm = hermitian_view(res);
    eigensolver<hermitian_matrix<double> > decomp2;
    diagonal_matrix<double> vals2;    tensor<double, 2> vecs2;



    decomp2(herm, vals2, vecs2);
    std::cerr << vals2 << std::endl;
    std::cerr << vecs2 << std::endl;
    std::cerr << std::endl;
    diagonal_matrix<double> vals2b = adjoint(vals2);
    std::cerr << "diagonal matrix transpose test" << std::endl;
    std::cerr << vals2b << std::endl;

    tensor<double, 2> mat2(n, n);   mat2.fill_zeros();
    tensor<complex<double>, 2> mat2c(n, n);   mat2c.fill_zeros();
    tensor<complex<double>, 2> mat4(n, n);   mat4.fill_zeros();
    double counter = 1.0;
    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=i; j<n; ++j)
        {
            mat2(i, j) = counter;
            mat2c(i, j) = counter;
            mat4(i, j) = counter;
            counter += 2.0;
        }
    }
    for(size_t i=1; i<n; ++i)
    {
        mat2(i, i-1) = counter;
        mat2c(i, i-1) = counter;
        mat4(i, i-1) = counter;
        counter += 1.0;
    }

    auto hess = upper_hessenberg_view(mat2);
    auto hessc = upper_hessenberg_view(mat2c);

    hess.order() = MATRIX_ORDERING::ROW_MAJOR;
    hessc.order() = MATRIX_ORDERING::ROW_MAJOR;
    eigensolver<upper_hessenberg_matrix<complex<double>> > decomp3c;
    eigensolver<upper_hessenberg_matrix<double>> decomp3;

    eigensolver<tensor<double, 2> > decomp4;
    eigensolver<tensor<complex<double>, 2> > decomp4c;

    diagonal_matrix<complex<double>> vals3(n, n);
    tensor<complex<double>, 2> vec3r(n, n);

    diagonal_matrix<complex<double>> vals3c(n, n);
    tensor<complex<double>, 2> vec3rc(n, n);
    std::cerr << "general test" << std::endl;

    tensor<complex<double>, 2> vec3l(n, n);
    tensor<complex<double>, 2> vec3lc(n, n);
    decomp4(mat2, vals3, vec3r, vec3l);
    decomp3c(hessc, vals3c, vec3rc, vec3lc, false);

    std::cerr << vals3 << std::endl;
    std::cerr << vals3c << std::endl;
    std::cerr << vec3r << std::endl;
    std::cerr << vec3l << std::endl;

    //std::cerr << "working_version" << std::endl;
    //std::cerr << vec3rc << std::endl;
    //std::cerr << vec3lc << std::endl;

    tensor<complex<double>, 2> temp3 = vals3*adjoint(vec3l);
    tensor<complex<double>, 2> res3 = vec3r*temp3;
    tensor<complex<double>, 2> resss = res3 - hess;
    std::cerr << res3 << std::endl;
    std::cerr << hess << std::endl;

    //std::cerr << res << std::endl;
    //std::cerr << resa << std::endl;
    */
    /* 
    int m = 2;  int k = 4;
    using size_type = blas_backend::size_type;
    using index_type = blas_backend::index_type;

    tensor<complex<double>, 1> a(m, [](int i){return complex<double>(i+1.0, 1.0);});
    tensor<complex<double>, 2> diag_mat_dense(m, k, [](int i, int j){return i == j ? complex<double>(i+1.0, 1.0) : 0.0;});
    tensor<complex<double>, 2> diag_mat_denser(k, m, [](int i, int j){return i == j ? complex<double>(i+1.0, 1.0) : 0.0;});
    tensor<complex<double>, 3> f(1, m, m, [](int l, int i, int j){return i == j ? complex<double>(i+1.0, 1.0) : 0.0;});

    std::vector<complex<double> > diag(m);  diag[0] = complex<double>(1, 0);   diag[1] = complex<double>(2, 1);
    diagonal_matrix<complex<double> > diagmat(diag, m, k);
    std::cerr << diagmat << std::endl;

    diagonal_matrix<complex<double> > diagmatb = conj(2.0*diagmat*4.0)/5.0;
    std::cerr << diagmatb << std::endl;

    tensor<complex<double>, 2> c = conj(2.0*(2.0*diag_mat_dense*2.0))*5.0;
    tensor<complex<double>, 2> g = (2.0*diag_mat_dense*diag_mat_denser);
    tensor<complex<double>, 2> gb = (2.0*diagmat*diag_mat_denser);

    tensor<complex<double>, 2> mat = trans(diag_mat_dense);
    //diag_mat_denser = trans(diag_mat_dense);
    std::cerr << diag_mat_dense << std::endl;
    //std::cerr << c << std::endl;
    std::cerr << f[0] << std::endl;
    std::cerr << "g: " << g << std::endl;
    std::cerr << "gb: " << gb << std::endl;
    //f.reinterpret_shape(2, 4) = conj(2.0*(2.0*diag_mat_dense));
    std::cerr << f[0] << std::endl;
    
    std::vector<std::tuple<index_type, index_type, complex<double> > > coo(2);
    coo[0] = std::make_tuple(0, 0, complex<double>(1.0, 1.0));
    coo[1] = std::make_tuple(1, 1, complex<double>(2.0, 1.0));

    csr_matrix<complex<double> > csrmat(coo);

    //csr_matrix<complex<double> > d = complex<double>(2.0, 0.0)*csrmat*4.0;
    std::cerr << "orig: " << csrmat << std::endl;
    //std::cerr << "algebra: " << d << std::endl;
    {
    std::ofstream os("csr.json");
    cereal::JSONOutputArchive archive(os);
    archive( CEREAL_NVP(csrmat));
    }

    csr_matrix<complex<double> > csrmatb;
    {
    std::ifstream is("csr.json");
    cereal::JSONInputArchive iarchive(is);
    iarchive( csrmatb);
    }
    std::cerr << csrmatb << std::endl;
    bool compare = (csrmatb.topology() == csrmat.topology());
    std::cerr << (compare ? "same" : "different") << std::endl;


    {
    std::ofstream os("data.json");
    cereal::JSONOutputArchive archive(os);
    archive( CEREAL_NVP(diag_mat_dense));
    }
    {
    std::ofstream os("data.xml");
    cereal::XMLOutputArchive archive(os);
    archive( CEREAL_NVP(diag_mat_dense));
    }


    tensor<complex<double>, 2> diag_mat_dense_b;
    {
    std::ifstream is("data.json");
    cereal::JSONInputArchive iarchive(is);
    iarchive( diag_mat_dense_b);
    }
    tensor<complex<double>, 2> diag_mat_dense_c;
    {
    std::ifstream is("data.xml");
    cereal::XMLInputArchive iarchive(is);
    iarchive( diag_mat_dense_c);
    }


    a = 2.0;
    auto reinterpreted = reinterpret_shape(diag_mat_dense_b, 2,2,2);
    auto slice = reinterpret_shape(diag_mat_dense[0], 2, 2);
    auto slice2 = slice[1];
   // hermitian_matrix<complex<double> > herm = hermitian_view(slice);
    std::cerr << slice[0][0] << std::endl;
    diag_mat_dense_b.fill(1.0);
    
    test(diag_mat_dense);
    std::cerr << reinterpreted << std::endl;
    std::cerr << diag_mat_dense_b << std::endl;
    std::cerr << diag_mat_dense_c(0, 1) << std::endl;
    std::cerr << a << std::endl;
*/

/*  
    size_t n = 2048;
    size_t _a = 3;   size_t _b = 5;   size_t _c = 4;

#ifdef TRANS_B
    std::cerr << "transposed dense ";
#else
    std::cerr << "standard dense ";
#endif

#ifdef LEFT_MULTIPLY
    std::cerr << "sparse*dense" << std::endl;
#else
    std::cerr << "dense*sparse" << std::endl;
#endif

    do_matrix_products(_a, _b, _c);
    //do_matrix_products(_c, _a, _b);
    //do_matrix_products(_b, _c, _a);
    //do_matrix_products(_a, _c, _b);
    //do_matrix_products(_b, _a, _c);
    //do_matrix_products(_c, _b, _a);
    
*/
    return 0;

}
