#include <linalg/linalg.hpp>
#include <typeinfo>
#include <iostream>
#include <chrono>

using namespace linalg;

template <typename T>
typename std::enable_if<std::is_same<T, int>::value, void>::type print_type(){}


template <typename T>
void runb(const T& a)
{
    std::cerr << a[0](1) << std::endl;
}

template <typename T> 
void run(const T& a)
{
    std::cerr << a[0].size(0) << std::endl;
    std::cout << a[0](0,1) << std::endl;
    auto t = a[0];
    runb(t);
}

int main()
{
    using namespace std::chrono;
    using size_type = blas_backend::size_type;
    using index_type = blas_backend::index_type;
    //high_resolution_clock::time_point t1 = high_resolution_clock::now();

    //std::vector<complex<double>> vals(3);   vals[0] = 1.0;  vals[1] = 1.0;  vals[2] = 1.0;
    //std::vector<index_type> colinds(3);     colinds[0] = 0; colinds[1] = 1; colinds[2] = 1;
    //std::vector<index_type> rowptr(3);      rowptr[0] = 0;  rowptr[1] = 1;  rowptr[2] = 3;
    csr_matrix<complex<double>> mat;//(vals, colinds, rowptr);

    //std::cerr << mat << std::endl;

    size_t n = 5; size_t m = 1000; size_t k = 3;

    tensor<complex<double>, 2> mat_dense(k, k, [](int i, int j){return i == j ? complex<double>(i+1.0, 1.0) : complex<double>(j,-1);});
    tensor<complex<double>, 2> result_matrix(k, k);
    tensor<complex<double>, 2> working_matrix(k, k);

    std::vector<std::tuple<index_type, index_type, complex<double>> > coo(k);
    size_type counter = 0;
    std::vector<complex<double>> dvals(k);
    for(size_type i=0; i<k; ++i)
    {
        coo[counter] = std::make_tuple(i, i, complex<double>(i+1.0));
        dvals[counter] = complex<double>(i+1.0);
        ++counter;
    }
    mat.init(coo);
    diagonal_matrix<complex<double>> diagmat(dvals);

    //result_matrix[0] = mat_dense[0]+mat_dense[1]-mat_dense[2];//(mat_dense*mat_dense)*5.0;//5.0*(trans(mat_dense)*2.0)*3.0;
    result_matrix = trans(5.0*conj(mat_dense));
    //result_matrix = ((mat_dense))*(conj(mat_dense));
    //result_matrix = (conj(mat_dense))*((mat_dense));

    //result_matrix = ((mat_dense))*(trans(mat_dense));
    //result_matrix = (trans(mat_dense))*((mat_dense));

    //result_matrix = (mat_dense)*(2.0*mat_dense);
    //result_matrix = (2.0*mat_dense)*(mat_dense);

    //result_matrix = ((mat_dense))*(2.0*conj(mat_dense));
    //result_matrix = (2.0*conj(mat_dense))*((mat_dense));



    //result_matrix = (2.0*conj(mat_dense))*(trans(mat_dense));
    //result_matrix = (trans(mat_dense))*(2.0*conj(mat_dense));

    //result_matrix = (conj(mat_dense))*(2.0*conj(mat_dense));
    //result_matrix = (2.0*conj(mat_dense))*(conj(mat_dense));

    //result_matrix = (mat_dense)*(mat_dense);
    //result_matrix = (trans(mat_dense))*(trans(mat_dense));
    //result_matrix = (conj(mat_dense))*(conj(mat_dense));
    //result_matrix = (2.0*conj(mat_dense))*(2.0*conj(mat_dense));
    for(size_t i=0; i<result_matrix.size(0); ++i){for(size_t j=0; j<result_matrix.size(1); ++j){std::cerr << result_matrix(i, j) << " ";}std::cerr << std::endl;}std::cerr << std::endl;

    result_matrix = adjoint(5.0*conj(mat_dense));//conj(conj(result_matrix));
    for(size_t i=0; i<result_matrix.size(0); ++i){for(size_t j=0; j<result_matrix.size(1); ++j){std::cerr << result_matrix(i, j) << " ";}std::cerr << std::endl;}std::cerr << std::endl;

    return 0;
    tensor<complex<double>, 3> ca(n, m, k, [](int _k , int i, int j, int _n){return complex<double>(static_cast<double>(i*_n+j+1+_k), i);}, k);
    //tensor<complex<double>, 3, cuda_backend> dca(5, 2, 4, []__device__(int  k , int i, int j, int n){return complex<double>(static_cast<double>(i*n+j*k+1), i);}, 2 );
    tensor<complex<double>, 1> conj_buffer;
    tensor<complex<double>, 1> working_buffer;

    tensor<complex<double>, 3> da(n, m, k, [](int _k , int i, int j, int _n){return complex<double>(static_cast<double>(i*_n+j+1+_k), i);}, k);
    tensor<complex<double>, 3> gfa(m, k, n);
    tensor<complex<double>, 3> res(1, m, m);
    //for(size_t i=0; i<500; ++i)
    {
        
        res[0] = contract(ca, 0, 2, da, 0, 2, working_buffer, conj_buffer);
        //res = ca[1]*da[0];
        auto perm = complex<double>(0.0, -1.0)*permute_dims(2.0*conj(ca), 2, 0);
        //expression_templates::tensor_permutation_3_expression<tensor<complex<double>, 3, cuda_backend> > perm(1.0, ca, 2);
        perm(gfa);
    }

    /*  

    tensor<complex<double>, 3> _resf(ca);


    for(size_t i=0; i<_resf.size(0); ++i)
    {
        for(size_t j=0; j<_resf.size(1); ++j)
        {
            for(size_t k=0; k<_resf.size(2); ++k)
            {
                std::cerr << _resf(i, j, k) << " ";
            }
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }

    std::cerr << std::endl;
    std::cerr << std::endl;
    _resf = gfa;

    for(size_t i=0; i<_resf.size(0); ++i)
    {
        for(size_t j=0; j<_resf.size(1); ++j)
        {
            for(size_t k=0; k<_resf.size(2); ++k)
            {
                std::cerr << _resf(i, j, k) << " ";
            }
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }*/
    /* 
    tensor<complex<double>, 2> fa(3, 3, 0.0);

    fa(0,0) = complex<double>(3.0, 1.0);
    fa(0,1) = 2.0;
    fa(0,2) = 5.0;
    fa(1,0) = 1.0;
    fa(1,1) =-6.0;
    fa(1,2) = 7.0;
    
    tensor<complex<double>, 2> temp(trans(fa));
    fa = temp;


    tensor<complex<double>, 3> ca(5, 3, 4, [](int k , int i, int j, int n){return complex<double>(static_cast<double>(i*n+j+1+k), i);}, 2 );
    tensor<complex<double>, 3> dca(5, 2, 4, [](int  k , int i, int j, int n){return complex<double>(static_cast<double>(i*n+j*k+1), i);}, 2 );
    tensor<complex<double>, 1> conj_buffer(5*3*4);
    tensor<complex<double>, 1> working_buffer(5*3*2);


    expression_templates::tensor_contraction_332<tensor<complex<double>, 3>, tensor<complex<double>, 3>> r(1.0, ca, dca, 1, working_buffer, conj_buffer, true, false);
    tensor<complex<double>, 2> resm(r);

    tensor<complex<double>, 2> res(3, 2);
    for(size_t i=0; i<3; ++i)
    {
        for(size_t j=0; j<2; ++j)
        {
            res(i, j) = 0.0;
            for(size_t k=0; k<5; ++k)
            {
                for(size_t l=0; l<4; ++l)
                {
                    res(i,j) += std::conj(ca(k, i, l))*(dca(k, j, l));
                }
            }
        }
    }
    
    */
    //tensor<complex<double>, 2, cuda_backend> resm(adjoint(fa)*tca);
    //tensor<complex<double>, 2> _resm(resm);

    //expression_templates::tensor_contraction_1_mt<tensor<complex<double>, 3, cuda_backend>, tensor<complex<double>, 2, cuda_backend> > r(1.0, ca, fa, 1, 0, false, true);

    //tensor<complex<double>, 3, cuda_backend> tempb = conj(5.0*ca);
    /*
    tensor<complex<double>, 3, cuda_backend> resf = contract(conj(fa), 0, ca, 1);

    tensor<complex<double>, 3> _resf(resf);


    for(size_t i=0; i<_resf.size(0); ++i)
    {
        for(size_t j=0; j<_resf.size(1); ++j)
        {
            for(size_t k=0; k<_resf.size(2); ++k)
            {
                std::cerr << _resf(i, j, k) << " ";
            }
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }
    */

    /*  
    for(size_t i=0; i<resm.size(0); ++i)
    {
        for(size_t j=0; j<resm.size(1); ++j)
        {
            std::cerr << resm(i, j) << " ";
        }
        std::cerr << std::endl;
    }
    std::cerr << std::endl;
    for(size_t i=0; i<res.size(0); ++i)
    {
        for(size_t j=0; j<res.size(1); ++j)
        {
            std::cerr << res(i, j) << " ";
        }
        std::cerr << std::endl;
    }*/

    /*
    auto cat = ca.reinterpret_shape(5, 12);
    auto dcat = dca.reinterpret_shape(4, 12);
    tensor<complex<double>, 2, cuda_backend> resn(cat*adjoint(dcat));
    tensor<complex<double>, 2> _resn(resn);

    std::cerr << std::endl;
    for(size_t i=0; i<_resn.size(0); ++i)
    {
        for(size_t j=0; j<_resn.size(1); ++j)
        {
            std::cerr << _resn(i, j) << " ";
        }
        std::cerr << std::endl;
    }
    */

}
